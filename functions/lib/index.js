"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const helper_1 = require("./helper");
const _ = require("lodash");
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
exports.nextDayContentForUser = functions.database.ref('/users/{userId}/{course_name}/plan/{array_id}/completed')
    .onUpdate((snap, context) => __awaiter(this, void 0, void 0, function* () {
    const userId = context.params.userId;
    const courseName = context.params.course_name;
    // get user firestor data
    const userData = yield helper_1.userFirestoreData(userId, courseName);
    // get higher weightage content
    const content = helper_1.getHigherWeightageContent(userData, courseName, userId);
    console.log('next day content', content);
    return Promise.resolve();
}));
exports.setContentBasedOnAssessment = functions.database.ref('users/{userId}/{course_name}/assessments/{array_id}')
    .onCreate((snap, context) => __awaiter(this, void 0, void 0, function* () {
    const userId = context.params.userId;
    const courseName = context.params.course_name;
    const newPriorities = snap.val();
    console.log(newPriorities);
    // get user firestor data
    const userData = yield helper_1.userFirestoreData(userId, courseName);
    // : { content_snap: Array<any> }
    if (!userData) {
        const domainContentBank = yield helper_1.getDomainContentBank(courseName);
        console.log('content bank', domainContentBank);
        const priority = {
            [courseName]: [newPriorities]
        };
        console.log('priority');
        console.log(priority);
        console.log('priority');
        const tempUserData = {
            priority: priority,
            tags: []
        };
        console.log('temp user object', tempUserData);
        // store user content_bank
        const userObject = yield helper_1.storeUserContentBank(domainContentBank, tempUserData, userId);
        console.log('user data store promise', userObject);
    }
    else {
        const userPriorities = userData.priority ? userData.priority : false;
        const priority = userPriorities[courseName];
        priority.push(newPriorities);
        const userCoursePriority = {
            [courseName]: priority
        };
        console.log('User updated priority', userCoursePriority);
        const userObject = yield helper_1.updateUserPriority(userCoursePriority, userId);
        console.log('user data store promise', userObject);
    }
    // get user firestor data
    const updatedUserData = yield helper_1.userFirestoreData(userId, courseName);
    console.log('updated user data', updatedUserData);
    // get higher weightage content
    const content = yield helper_1.getHigherWeightageContent(updatedUserData, courseName, userId);
    console.log('Highehest weighted content', content);
}));
exports.triggerOnUserFeedback = functions.database.ref('users/{userId}/content_feedback/{feebback}')
    .onCreate((snap, context) => __awaiter(this, void 0, void 0, function* () {
    const userId = context.params.userId;
    const courseName = yield helper_1.userCurrentCourseName(userId);
    console.log('user course name main function', courseName);
    // user feedback data
    const feedbackObj = snap.val();
    // feedback weight
    const extraWeight = feedbackObj.extra_weight;
    // feedaback related content  tags
    const contentTags = feedbackObj.tags;
    // get user firestor data
    const userData = yield helper_1.userFirestoreData(userId, courseName);
    console.log('updated user data', userData);
    // user tags
    const userTags = userData.tags;
    const tempObj = {};
    // check user tags 
    if (userTags === undefined) {
        _.each(contentTags, (tag) => {
            tempObj[tag] = extraWeight;
        });
    }
    else {
        _.forEach(contentTags, (tag) => {
            if (tag in userTags) {
                tempObj[tag] = userTags[tag] + extraWeight;
            }
            else {
                tempObj[tag] = extraWeight;
            }
        });
    }
    // user tag merge to userdata  
    const userTagUpdated = yield helper_1.updateUserTags(tempObj, userId);
    console.log('User tyag updated', userTagUpdated);
    // get user firestor data
    const updatedUserData = yield helper_1.userFirestoreData(userId, courseName);
    console.log('updated user data', updatedUserData);
    // update user content weightage
    const updatedWeightageContent = helper_1.updateUserContentWeightage(updatedUserData, userId);
    console.log('User updated content weightage', updatedWeightageContent);
    // get user firestor data
    const updatedUserContentData = yield helper_1.userFirestoreData(userId, courseName);
    console.log('updated user data', updatedUserData);
    // get higher weightage content
    const content = yield helper_1.getHigherWeightageContent(updatedUserContentData, courseName, userId);
    console.log('Highehest weighted content', content);
}));
//# sourceMappingURL=index.js.map