import * as functions from 'firebase-functions';
import { 
  userFirestoreData, 
  getHigherWeightageContent,
  getDomainContentBank,
  storeUserContentBank,
  updateUserPriority,
  updateUserTags,
  updateUserContentWeightage,
  userCurrentCourseName
} from './helper';
import * as _ from "lodash";

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });


export const nextDayContentForUser = functions.database.ref('/users/{userId}/{course_name}/plan/{array_id}/completed')
.onUpdate(async (snap, context) => {
  const userId = context.params.userId;
  const courseName = context.params.course_name;
  // get user firestor data
  const userData = await userFirestoreData(userId, courseName);
  // get higher weightage content
  const content = getHigherWeightageContent(userData, courseName, userId);
  console.log('next day content', content);
  return Promise.resolve();
});

interface UserData {
  content_snap: Array<any>,
  priority: Object,
  tags: Array<any>
}

exports.setContentBasedOnAssessment = functions.database.ref('users/{userId}/{course_name}/assessments/{array_id}')
.onCreate(async (snap, context) => {
  const userId = context.params.userId;
  const courseName = context.params.course_name;
  const newPriorities = snap.val();
  console.log(newPriorities); 
  // get user firestor data
  const userData = await userFirestoreData(userId, courseName);
  // : { content_snap: Array<any> }
  if (!userData) {
    const domainContentBank = await getDomainContentBank(courseName);
    console.log('content bank', domainContentBank);
    const priority = {
      [courseName]: [newPriorities]
    }
    console.log('priority')
    console.log(priority)
    console.log('priority')
    const tempUserData = {
      priority: priority,
      tags: []
    }
    console.log('temp user object', tempUserData);
    // store user content_bank
    const userObject = await storeUserContentBank(domainContentBank, tempUserData, userId);
    console.log('user data store promise', userObject);

  } else {
    const userPriorities = userData.priority ? userData.priority : false;
    const priority = userPriorities[courseName];
    priority.push(newPriorities);
    const userCoursePriority = {
      [courseName]: priority
    }
    console.log('User updated priority', userCoursePriority);
    const userObject = await updateUserPriority(userCoursePriority, userId);
    console.log('user data store promise', userObject);
  }
  // get user firestor data
  const updatedUserData = await userFirestoreData(userId, courseName);
  console.log('updated user data', updatedUserData);

  // get higher weightage content
  const content = await getHigherWeightageContent(updatedUserData, courseName, userId);
  console.log('Highehest weighted content', content);
});

export const triggerOnUserFeedback = functions.database.ref('users/{userId}/content_feedback/{feebback}')
.onCreate(async (snap, context) => {
  const userId = context.params.userId;
  const courseName = await userCurrentCourseName(userId);
  console.log('user course name main function', courseName);
  // user feedback data
  const feedbackObj = snap.val();
  // feedback weight
  const extraWeight = feedbackObj.extra_weight;
  // feedaback related content  tags
  const contentTags = feedbackObj.tags;
  // get user firestor data
  const userData = await userFirestoreData(userId, courseName);
  console.log('updated user data', userData);

   // user tags
   const userTags = userData.tags;
   const tempObj = {};
   // check user tags 
   if (userTags === undefined) {
       _.each(contentTags, (tag) => {
           tempObj[tag] = extraWeight;
       });
   } else {
      _.forEach(contentTags, (tag) => {
          if (tag in userTags) {
              tempObj[tag] = userTags[tag] + extraWeight;
          } else {
              tempObj[tag] = extraWeight;
          }
      });
   }
  // user tag merge to userdata  
  const userTagUpdated = await updateUserTags(tempObj, userId);
  console.log('User tyag updated', userTagUpdated);

  // get user firestor data
  const updatedUserData = await userFirestoreData(userId, courseName);
  console.log('updated user data', updatedUserData);

  // update user content weightage
  const updatedWeightageContent = updateUserContentWeightage(updatedUserData, userId);
  console.log('User updated content weightage', updatedWeightageContent);

  // get user firestor data
  const updatedUserContentData = await userFirestoreData(userId, courseName);
  console.log('updated user data', updatedUserData);

  // get higher weightage content
  const content = await getHigherWeightageContent(updatedUserContentData, courseName, userId);
  console.log('Highehest weighted content', content);
});