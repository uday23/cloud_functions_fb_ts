"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
const _ = require("lodash");
admin.initializeApp();
const FIRESTORE = admin.firestore();
const REALTIMEDB = admin.database();
exports.getDomainContentBank = (domain) => {
    return FIRESTORE.collection('content_bank').where('domain', '==', domain).get()
        .then((content_snapshot) => {
        const contentBank = [];
        content_snapshot.forEach((doc) => {
            const content = doc.data();
            content.id = doc.id;
            contentBank.push(content);
        });
        return contentBank;
    });
};
exports.storeUserContentBank = (contentBank, userData, userId) => {
    const finalDomainContaint = [];
    console.log('inside store user content', userData.priority);
    contentBank.forEach(content => {
        const dataItem = content;
        let tagWeight = 0;
        let userTags = userData.tags;
        _.forEach(dataItem.tags, (tag) => {
            if (userTags[tag]) {
                tagWeight += userTags[tag];
            }
        });
        const payload = {
            content_id: content.id,
            content_label: dataItem.label_first,
            domain: dataItem.domain,
            initial_weightage: dataItem.weightage,
            symptom: dataItem.symptom,
            tags: dataItem.tags,
            tags_weightage: tagWeight,
            overall_weightage: dataItem.weightage + tagWeight,
        };
        console.log('content', payload);
        finalDomainContaint.push(payload);
    });
    userData.content_snap = finalDomainContaint;
    console.log('User content_snap', userData.priority);
    console.log('User content_snap', userData.tags);
    console.log('User content_snap', userData.content_snap);
    return FIRESTORE.collection('users').doc(userId).set(userData)
        .then(() => Promise.resolve(userData))
        .catch(() => Promise.reject(userData));
};
exports.userFirestoreData = (userId, courseName) => __awaiter(this, void 0, void 0, function* () {
    const contentBank = yield exports.getDomainContentBank(courseName);
    console.log('Content from meta content', contentBank);
    let userData;
    yield FIRESTORE.collection('users').doc(userId).get().then(doc => {
        userData = doc.data();
        console.log("user data in function", userData);
        return userData;
    });
    console.log("user data fom firestore", userData);
    let userObject;
    if (!userData) {
        userObject = userData;
    }
    else {
        userObject = yield exports.storeUserContentBank(contentBank, userData, userId);
    }
    console.log('Updated user object', userObject);
    return Promise.resolve(userObject);
});
exports.getHigherWeightageContent = (data, courseName, userId) => {
    // user data in firestore
    const userData = data;
    // user priiorities as per assessments
    const userAssessmentPriorities = userData.priority;
    console.log("user priority course", userAssessmentPriorities);
    // fetch course related priorities
    const diffPriority = userAssessmentPriorities[courseName];
    console.log("user priority course", diffPriority);
    // fecth last updated priorities
    const priorityArray = _.last(diffPriority);
    console.log("user priority", priorityArray);
    // symptoms order
    const symptomsOrder = priorityArray.order;
    // user content snap shot as per course
    const userContentSnap = userData.content_snap;
    // user used content id array
    const userContentHistory = userData.content_history;
    // removed used content by user
    const filteredContent = _.filter(userContentSnap, (content) => {
        const index = _.findIndex(userContentHistory, (id) => content.content_id === id);
        // console.log('content index', index, content);
        if (index < 0) {
            return content;
        }
    });
    let contentArray;
    for (let index = 0; index < symptomsOrder.length; index++) {
        console.log('symptomsOrder', symptomsOrder[index]);
        const symptomObj = symptomsOrder[index];
        const symptom = symptomObj.symptom;
        contentArray = _.filter(filteredContent, (content) => content.symptom === symptom);
        if (contentArray.length > 0) {
            break;
        }
    }
    // find highest weightage content from filtered content
    const nextDayContent = _.maxBy(contentArray, (content) => content.overall_weightage);
    let endDay;
    // get current course plan
    return REALTIMEDB.ref(`users/${userId}/${courseName}/plan`).once('value', (planSnap) => {
        const planSnapData = planSnap.val();
        let updatedPlan = [];
        if (!planSnapData) {
            endDay = Object.assign({}, nextDayContent, { day: 1, completed: false });
            updatedPlan.push(endDay);
        }
        else {
            const lastDay = _.last(planSnapData);
            if (!lastDay.completed) {
                updatedPlan = _.dropRight(planSnapData);
                endDay = Object.assign({}, nextDayContent, { day: lastDay.day, completed: false });
            }
            else {
                updatedPlan = planSnapData;
                endDay = Object.assign({}, nextDayContent, { day: lastDay.day + 1, completed: false });
            }
            updatedPlan.push(endDay);
        }
        console.log('updated latest day on firebase', endDay);
        // update the course plan
        return admin.database().ref(`users/${userId}/${courseName}/plan`).set(updatedPlan)
            .then(() => Promise.resolve(endDay))
            .catch(() => Promise.reject(endDay));
    });
};
exports.updateUserPriority = (userCoursePriority, userId) => {
    return FIRESTORE.collection('users').doc(userId).set({
        priority: userCoursePriority
    }, {
        merge: true
    }).then(() => Promise.resolve('User priority updated'))
        .catch(() => Promise.reject('Unable to update user priority'));
};
exports.updateUserTags = (userTags, userId) => {
    return FIRESTORE.collection('users').doc(userId).set({
        tags: userTags
    }, {
        merge: true
    }).then(() => Promise.resolve('User tags updated'))
        .catch(() => Promise.reject('Unable to update user tags'));
};
exports.updateUserContent = (contentBank, userId) => {
    return FIRESTORE.collection('users').doc(userId).set({
        content_snap: contentBank
    }, {
        merge: true
    }).then(() => Promise.resolve('User content updated'))
        .catch(() => Promise.reject('Unable to update user content'));
};
exports.updateUserContentWeightage = (userData, userId) => __awaiter(this, void 0, void 0, function* () {
    const updatedUserSnap = userData;
    // add tag weghtage to content
    const content_snap = updatedUserSnap.content_snap;
    const finalpayload = [];
    // update overall weight depend upon tag weight
    content_snap.forEach(content => {
        let tagWeight = 0;
        _.forEach(content.tags, (tag) => {
            if (updatedUserSnap.tags[tag]) {
                tagWeight += updatedUserSnap.tags[tag];
            }
        });
        content.overall_weightage = tagWeight + content.initial_weightage;
        content.tags_weightage = tagWeight;
        finalpayload.push(content);
    });
    console.log("+++++++++++++");
    console.log(finalpayload);
    console.log("+++++++++++++");
    // update user content_snap
    yield exports.updateUserContent(finalpayload, userId);
    return finalpayload;
});
exports.userCurrentCourseName = (userId) => __awaiter(this, void 0, void 0, function* () {
    let courseName;
    yield REALTIMEDB.ref(`users/${userId}/currentCourseName`).once('value', (snap) => {
        courseName = snap.val();
        console.log('coursename', snap.val());
        return snap.val();
    });
    console.log('User Course name', courseName);
    return Promise.resolve(courseName);
});
//# sourceMappingURL=helper.js.map